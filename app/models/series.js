var mongoose = require('mongoose');

module.exports = mongoose.model('Series', {
	min : {type : Number, default: 0}
	,max : {type : Number, default: 0}
	,title : {type : String, default: ''}
	,chart_type : {type : String, default: 'Line'}
	,data : []
});