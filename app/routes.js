var Series = require('./models/series');

function getSeries(res){
	Series.find(function(err, series) {

		// if there is an error retrieving, send the error. nothing after res.send(err) will execute
		if (err)
			res.send(err)
		
		res.json(series); // return all series in JSON format
	});
};

function getAllSeries(res, insert_series){
	Series.find(function(err, series) {

		// if there is an error retrieving, send the error. nothing after res.send(err) will execute
		if (err)
			res.send(err)

		var all = {all: series, last:insert_series}; // return all and last series in JSON format
		res.json(all);
	});
};

module.exports = function(app) {

	// api ---------------------------------------------------------------------
	// get all series
	app.get('/api/series/:series_id', function(req, res) {
		Series.findById(req.params.series_id, function(err, series) {
            if (err)
                res.send(err);
			
			console.log("App listening on port " + req.params.series_id + " dsfsdf");
            res.json(series);
        });
	});
	
	app.get('/api/series', function(req, res) {
		// use mongoose to get all series in the database
		
		console.log("App listening on port ");// + req.params.series_id + " dsfsdf");
		getSeries(res);
	});
	
	
	// create series and send back all series after creation
	app.post('/api/series', function(req, res) {
	
		// create a series, information comes from AJAX request from Angular
		Series.create({
			title : req.body.title,
			max: req.body.max,
			min: req.body.min,
			chart_type: req.body.chart_type,
			data : req.body.data,
			done : false
		}, function(err, series) {
			if (err)
				res.send(err);
			
			// return last series entry
			getAllSeries(res, series);
		});

	});

	// delete a series
	app.delete('/api/series/:series_id', function(req, res) {
		Series.remove({
			_id : req.params.series_id
		}, function(err, series) {
			if (err)
				res.send(err);

			getSeries(res);
		});
	});

	// application -------------------------------------------------------------
	app.get('*', function(req, res) {
		res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
	});
};