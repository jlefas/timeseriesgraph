angular.module('seriesService', [])

	// super simple service
	// each function returns a promise object 
	.factory('Series', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/series');
			},
			getSingle : function(id) {
				return $http.get('/api/series/' + id);
			},
			create : function(seriesData) {
				return $http.post('/api/series', seriesData);
			},
			delete : function(id) {
				return $http.delete('/api/series/' + id);
			}
		}
	}]);