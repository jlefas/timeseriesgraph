angular.module('seriesController', ['highcharts-ng'])

	// inject the Series service factory into our controller
	.controller('mainController', ['$scope','$http','Series', function($scope, $http, Series) {
		//Graph type dropdown
		$scope.feed = {};
		$scope.feed.configs = [{'name': 'Line', 'value': 'line'},
                               {'name': 'Scatter', 'value': 'scatter'},
                               {'name': 'Bar', 'value': 'bar'}];
		$scope.feed.config = $scope.feed.configs[0].value;
		$scope.formData = {};
		$scope.loading = true;
		
		// GET 
		// when landing on the page, get all series and show them
		// use the service to get all the series
		Series.get()
			.success(function(data) {
				$scope.series = data;
				$scope.loading = false;
			});

		// CREATE 
		// when submitting the add form, send the text to the node API
		$scope.createSeries = function() {

			// validate the formData to make sure that something is there
			// if form is empty, nothing will happen
			if ($scope.formData.title != undefined && $scope.formData.min != undefined && $scope.formData.max != undefined) {
				$scope.loading = true;
				
				var min = $scope.formData.min;
				var max = $scope.formData.max;
				var title = $scope.formData.title;
				var chart_type = $scope.formData.chart_type;
				
				var data = returnTimeSeries(min, max, chart_type);
				
				// call the create function from our service (returns a promise object)
				//Series.create($scope.formData)
				Series.create({
								min : min
								,max : max
								,title : title
								,chart_type : chart_type
								,data : data
							})

					// if successful creation, call our get function to get all the new series
					.success(function(data) {
						$scope.loading = false;
						$scope.formData = {}; // clear the form so our user is ready to enter another
						$scope.series = data.all; // assign our new list of series
						
						graphSeriesById(data.last, $scope)
						
					});
			}
		};

		// DELETE 
		// delete a series after checking it
		$scope.deleteSeries = function(id) {
			$scope.loading = true;

			Series.delete(id)
				// if successful creation, call our get function to get all the new series
				.success(function(data) {
					$scope.loading = false;
					$scope.series = data; // assign our new list of series
				});
		};
		
		//PLOT 
		$scope.graphSeries = function () {
			
			Series.getSingle($scope.formData.seriesId)
				// if successful creation, call our get function to get all the new series
				.success(function(data) {
					$scope.loading = false;
					graphSeriesById(data, $scope); // assign new series
				});
			
			
		}
		
				
		// initialise plot then hide
		$scope.chartConfig = {	
			options: {
				chart: {
					type: 'bar'
				}	
			},
			xAxis: {
				type: 'datetime',
				dateTimeLabelFormats: { 
					day: '%b %e',
					week: '%b %e'
				}
			},	
			series: [{
				data: []	
			}],
			title: {
				text: ''
			},

			loading: false
		};
		$scope.chartConfig.series = [];
		
		
	}]);
	
	function graphSeriesById(mSeries, t) {
		//var mSeries;
		//for(i=0; i < t.series.length; i++ ){
		//	if(t.series[i]._id == seriesId){
		//		mSeries = t.series[i];
		//	}
		//}
		
		// format the data to plot
		var res_data = [];
		var series_data = [];
		for(j=0; j < mSeries.data.length; j++){
			for(i=0; i < mSeries.data[j].series.length; i++){
				res_data.push([(Number(mSeries.data[j].series[i][0])), Number(mSeries.data[j].series[i][1])]);
				//alert(new Date(Number(mSeries.data[0].series[i][0])))
			}
			series_data.push({	name: 'plot ' + j
								,color: 'rgba('+Math.floor(Math.random() * 244)+', '+Math.floor(Math.random() * 244)+', '+Math.floor(Math.random() * 244)+', .5)'
								,data: res_data
							})
			res_data = [];
		}
		
		// assign plot data and config
		t.chartConfig = {
			options: {
				chart: {
					type: mSeries.chart_type.toLowerCase()
				}	
			},
			xAxis: {
				type: 'datetime',
				dateTimeLabelFormats: { 
					day: '%b %e',
					week: '%b %e'
				}
			},
			series: series_data,
			title: {
				text: mSeries.title
			},

			loading: false
		};
	}
	
	// create time series data
	function returnTimeSeries(min, max, type){
		var date_str = "2015-11-";
		var res = max - min;
		var ts_array = [];
		var ts_array_scatter = [];
		
		for(i=0; i < 30; i++){
			ts_array[i] = [Date.UTC(2015, 10, (i+1)), (Math.random() * res + max)];
			
			if(type == 'scatter') //second object for scatter plots
				ts_array_scatter[i] = [Date.UTC(2015, 10, (i+1)), (Math.random() * res + max)]; 
		}
		
		if(type == 'scatter')
			ts = [{series: ts_array}, {series: ts_array_scatter}];
		else
			ts = [{series: ts_array}];
		
		return ts;
	}